/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.urbizone;

/**
 *
 * @author Jérôme Fafchamps <jerome@fafchamps.be>
 * @date juillet 2014
 */
public class Config {
    
    private String Login = "replicatorbe";
    private String password; 
    private boolean etat; 

    public Config() {
    }

    public String getLogin() {
        return Login;
    }

    public void setLogin(String Login) {
        this.Login = Login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isEtat() {
        return etat;
    }

    public void setEtat(boolean etat) {
        this.etat = etat;
    }
    
   
}