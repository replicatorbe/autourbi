package com.urbizone;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends Activity
{
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    }
    
    public void sendForm (View button) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(NSD_SERVICE);
        alert.show();
    }
}
